<?php

namespace App\Http\Controllers;

use App\livrosControle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LivrosControleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $dados = livroscontrole::all();
        return view("graficos.metas",compact('dados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('graficos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          
        DB::table('create_livros_table')->insert([

            'titulo'=> $request->titulo,
            'nome_editora'=> $request->nome,
            'tipo_livro'=> $request->tipo_livro,
            'emprestado' => $request->emprestado,
        
         ]);

        
       
         return redirect()->route('graficos.metas');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\livrosControle  $livrosControle
     * @return \Illuminate\Http\Response
     */
    public function show(livrosControle $livrosControle)
    {
        
       
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\livrosControle  $livrosControle
     * @return \Illuminate\Http\Response
     */
    public function edit(livrosControle $livrosControle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\livrosControle  $livrosControle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, livrosControle $livrosControle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\livrosControle  $livrosControle
     * @return \Illuminate\Http\Response
     */
    public function destroy(livrosControle $livrosControle)
    {
        //
    }
}
