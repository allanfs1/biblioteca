<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_livros_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('tutulo',120)->nullable();
            $table->String('nome_editora',70)->nullable();
            $table->String('tipo_livro',40)->nullable();
            $table->boolean('emprestado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livros');
    }
}
